package com.ko.springdemo;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class BeanScopeDemoApp {

	public static void main(String[] args) {

		// load the spring config file
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("beanScopeTwo-applicationContext.xml");

		// retrieve bean from spring container
		Coach theCoach = context.getBean("myCoach", Coach.class);

		Coach alphaCoach = context.getBean("myCoach", Coach.class);

		// check if they are the same bean
		boolean result = (theCoach == alphaCoach);

		// print out the results
		System.out.println("\nPointing to the same object: " + result);

		System.out.println("\nMemory location for theCoach: " + theCoach);
		System.out.println(theCoach.getDailyAdvice());
		System.out.println(theCoach.getDailyProverb());

		System.out.println("\nMemory location for alphaCoach: " + alphaCoach + "\n");
		System.out.println(alphaCoach.getDailyAdvice());
		System.out.println(theCoach.getDailyProverb());
		
		// close the context
		context.close();

	}

}
