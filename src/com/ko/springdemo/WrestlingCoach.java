package com.ko.springdemo;

public class WrestlingCoach implements Coach {

	// define a private field for the dependency
	private ProverbService proverbService;

	// define a constructor for dependency injection
	public WrestlingCoach(ProverbService theProverbService) {
		proverbService = theProverbService;
	}

	@Override
	public String getDailyAdvice() {

		return "Watch Hulk Hogan fights";
	}

	@Override
	public String getDailyProverb() {
		// use my proverbService to get a saying
		return proverbService.getProverb();
	}

}
