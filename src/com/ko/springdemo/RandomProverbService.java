package com.ko.springdemo;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RandomProverbService implements ProverbService {
	
	// Array with three proverbs
	List<String> proverbList = new ArrayList<>();
	
	// no-arg constructor
	public RandomProverbService() {
	
		proverbList.add("Keep your eye on the ball and your head in the game.");
		proverbList.add("He who leaves the game, loses.");
		proverbList.add("The ball always looks for the best player.");
	}



	@Override
	public String getProverb() {
		Random randomizer = new Random();
		String random = proverbList.get(randomizer.nextInt(proverbList.size()));
		return random;
	}

}
