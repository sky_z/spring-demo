package com.ko.springdemo;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SetterDemoApp {

	public static void main(String[] args) {

		// load the spring config file
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

		// retrieve the bean from the spring container
		BaseballCoach theCoach = context.getBean("myBaseballCoach", BaseballCoach.class);

		// call methods on the bean
		// ... let's come back to this ...
		System.out.println(theCoach.getDailyAdvice());

		System.out.println(theCoach.getDailyProverb());

		// call the new methods to get the literal values
		System.out.println(theCoach.getName());

		System.out.println(theCoach.getLocation());
		// close the context
		context.close();
	}

}
