package com.ko.springdemo;

public class FunnyProverbService implements ProverbService {

	@Override
	public String getProverb() {
		return "To lengthen your life, shorten your meals.";
	}

}
