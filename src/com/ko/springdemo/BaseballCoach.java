package com.ko.springdemo;

public class BaseballCoach implements Coach {

	private ProverbService proverbService;

	// add new fields for name and location
	private String name;
	private String location;

	// no-arg constructor
	public BaseballCoach() {
		System.out.println("BaseballCoach: inside no-arg constructor");
	}

	// the setter method
	public void setProverbService(ProverbService proverbService) {
		System.out.println("BaseballCoach: inside setter method - setProverbService");
		this.proverbService = proverbService;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		System.out.println("BaseballCoach: inside setter method - setName");
		this.name = name;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		System.out.println("BaseballCoach: inside setter method - setLocation");
		this.location = location;
	}

	@Override
	public String getDailyAdvice() {
		return "Buy a good baseball cap";
	}

	@Override
	public String getDailyProverb() {
		return proverbService.getProverb();
	}

}
