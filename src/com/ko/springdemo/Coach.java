package com.ko.springdemo;

public interface Coach {
	
	public String getDailyAdvice();
	
	public String getDailyProverb();

}
