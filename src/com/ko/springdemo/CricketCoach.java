package com.ko.springdemo;

public class CricketCoach implements Coach {

	private ProverbService proverbService;

	// no-arg constructor
	public CricketCoach() {
	}

	public CricketCoach(ProverbService proverbService) {
		this.proverbService = proverbService;
	}

	@Override
	public String getDailyAdvice() {
		return "Find out what cricket is through internet";
	}

	@Override
	public String getDailyProverb() {
		return "In Cricket we never say: " + proverbService.getProverb();
	}

	// add an init method
	public void doMyStartupStuff() {
		System.out.println("CricketCoach: inside method doMyStartupStuff");
	}

	// add a destroy method
	public void doMyCleanupStuff() {
		System.out.println("CricketCoach: inside method doMyCleanupStuff");
	}

}
