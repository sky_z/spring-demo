package com.ko.springdemo;

public class MyApp {

	public static void main(String[] args) {

		// create object
		Coach theCoach = new CricketCoach();

		// use the object
		System.out.println(theCoach.getDailyAdvice());
	}

}
